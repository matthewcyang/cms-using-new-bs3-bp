module.exports = {
    build_dir: 'build',
    compile_dir: 'bin',
    server: {
        dir: process.cwd() + '/server',
        config: {
            logger: {
                transports: null
            }
        }
    },
    app_files: {
        manifest:'src/manifest.json',
        js: [
            'src/background.js',
            'src/**/!(spec|e2e)*.js',
            '!src/assets/**/*.js',
            '!src/spec/helpers/*.js'
        ],
        assets: [
            'src/assets/**/*'
        ],

        jsunit: [
            'src/app/**/spec.*.js'
        ],

        templates: [
            '**/*.tpl.html'
        ],

        html: [ 'src/index.html' ],
        less: 'src/less/main.less'
    },
    test_files: {
        client: {
            app:[
                'vendor/angular/angular.js',
                'src/app/**/!(spec|e2e)*.js'
            ],
            spec:[
                'vendor/angular-mocks/angular-mocks.js',
                'src/**/spec.*.js',
            ],
            e2e:[
                'src/**/e2e.*.js',
            ]
        },
        server: {
            spec:[
                '<%= server.dir %>/**/*.spec.js',
            ],
            e2e:[
                '<%= server.dir %>/**/*.e2e.js',
            ]
        }
    },
    vendor_files: {
        js: [
            'vendor/angular/angular.js',
            'vendor/angular-bootstrap/ui-bootstrap.min.js',
            'vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'vendor/angular-ui-router/release/angular-ui-router.js',
            'vendor/angular-ui-utils/modules/route/route.js',
            'vendor/firebase/firebase.js',
            'vendor/firebase-simple-login/firebase-simple-login.js',
            'vendor/angularfire/angularfire.min.js',
            'vendor/angular-animate/angular-animate.min.js',
            'vendor/angular-growl/build/angular-growl.min.js',
            'vendor/textAngular/textAngular-sanitize.min.js',
            'vendor/textAngular/textAngular.min.js'
        ],
        /** This can be LESS or just reg CSS files **/
        less: [
            'vendor/angular/angular-csp.css',
            // 'vendor/angular-growl/build/angular-growl.min.css',
            'vendor/bootstrap/less/bootstrap.less'
        ],
        assets: []
    }
};
