var express = require('express'),
    http = require('http'),
    app = express(),
    server = http.createServer(app)
 
app.use(express.static(__dirname + '/build'));
 
app.get('/', function(req, res) {
    res.send({});
});
 
app.use(
    function(err, req, res, next){
        console.error(err.stack);
        res.send(500);
    }
);
 
exports = module.exports = server;
exports.use = function() {
  app.use.apply(app, arguments);
};