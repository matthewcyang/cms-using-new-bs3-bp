angular.module( 'mycms', [
	'mycms.services',
	'mycms.login',
	'mycms.dashboard',
	'mycms.postview',
	'ui.router',
	'ui.bootstrap',
	'ui.bootstrap.tpls',
	'ui.bootstrap.collapse',
	'ngAnimate',
	'angular-growl',
	'textAngular'

])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider, growlProvider ) {
	$urlRouterProvider.otherwise( '/login' );
	growlProvider.globalTimeToLive(3000);
})

.run(['$rootScope', '$location', 'loginService', '$state', 'waitForAuth',  function ( $rootScope, $location, loginService, $state, waitForAuth ) {

	$rootScope.loginObj = loginService.init('/login');

	// enumerate routes that don't need authentication
	var routesThatDontRequireAuth = ['/login','/public'];

	// check if current location matches route  
	var routeClean = function (route) {
		// console.log("route ",route);
		return _.find(routesThatDontRequireAuth, function (noAuthRoute) {
		    return _.str.startsWith(route, noAuthRoute);
		});
	};

	$rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
		// if route requires auth and user is not logged in

		// if(from.url === '^') {
			// if( !!(loginService.loggedin()) ){
			//	waitForAuth.then(function(){
			//		$location.path('/dashboard/postlisting');
			//	});
   //          }
   //          else {
   //              $rootScope.error = null;
   //              $location.path('/login');
   //          }
         // }


		if( !!$state.includes("login") && !routeClean(to.url) && !loginService.loggedin() ){

			console.log("at login and not logged in");
			console.log(to.url + " not public");
			ev.preventDefault();
		}

		if ( !$state.includes("login") && !routeClean(to.url) && !loginService.loggedin() ) {
			// redirect back to login
			console.log("not at login and not logged in");
			console.log(to.url + " not public");
			ev.preventDefault();
			$state.go("login");
			// $location.path('/login');
		}

	});
/*
	$rootScope.$on('$stateNotFound', function(){
		console.log("state not found");
		if( !!(loginService.loggedin()) ){
				waitForAuth.then(function(){
					$location.path('/dashboard/postlisting');
				});
			}
			else {
				$rootScope.error = null;
				$location.path('/login');
			}
	});
*/

	$rootScope.$on('$firebaseSimpleLogin:login', function() {
		console.log("$firebaseSimpleLogin:login fired ");
		waitForAuth.then(function(){
			if( $state.includes("login") ){
				$location.path('/dashboard/postlisting');
			}
		});
	});

	$rootScope.$on('$firebaseSimpleLogin:logout', function() {
		console.log("USER LOGGED OUT");
		// $state.go('dashboard');
		$location.path('/login');
    });

	$rootScope.$on('$firebaseSimpleLogin:error', function(e) {
		console.log("error ",e);
    });

}])


.controller( 'AppCtrl', function AppCtrl ( $rootScope, $scope, $location, loginService, waitForAuth, $state ) {
	$scope.$state = $state;
	$scope.stuff = {};

	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		if ( angular.isDefined( toState.data.pageTitle ) ) {
			// console.log(toState.data.pageTitle);
			$scope.stuff.pageTitle = toState.data.pageTitle + ' | mycms' ;
		}
	});
	waitForAuth.then(function(){
		if($scope.loginObj.user!==null){
			$scope.stuff.uname = $scope.loginObj.user.displayName;
		}
	});

	$scope.stuff.loggedin = loginService.loggedin;

	$scope.stuff.logOut =  loginService.logout;
})


.constant('FBURL', 'https://mcycms.firebaseio.com/')
.constant('_', window._)

;
