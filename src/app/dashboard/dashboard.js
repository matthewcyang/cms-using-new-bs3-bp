
angular.module( 'mycms.dashboard', [
  'mycms.services',
  'ui.router',
  'angular-growl',
  'textAngular'
])

.config(function config( $stateProvider ) {
  $stateProvider
  .state( 'dashboard', {
    abstract: true,
    url: '/dashboard',
    views: {
      "main": {
        controller: 'DashboardCtrl',
        templateUrl: 'dashboard/templates/dashboard.tpl.html'
      }
    },

    data:{ pageTitle: 'dashboard' }
  })

  .state('dashboard.postlisting', {
    url: '/postlisting',
    parent: 'dashboard',
    views:{
      "content": {
        controller: 'PostListCtrl',
        templateUrl: "dashboard/templates/postsummary.tpl.html"
      }
    }
  })

  .state('dashboard.newpost', {
    url: '/newpost',
    parent: 'dashboard',
    views:{
      "content": {
        controller: 'NewCtrl',
        templateUrl: "dashboard/templates/postEdit.tpl.html"
      }
    }
  })

  .state('dashboard.edit', {
    url: '/edit/:id',
    parent: 'dashboard',
    views:{
      "content": {
        controller: 'EditCtrl',
        templateUrl: "dashboard/templates/postEdit.tpl.html"
      }
    }
  })
  ;//end states

}) //end config

.run([ '$state', '$rootScope', function ( $state, $rootScope ) {
	// console.log('dashboard loginobj', $rootScope.loginObj);
}]) //end run


/**
 * And of course we define a controller for our route.
 */
.controller( 'DashboardCtrl', function DashboardController( $rootScope, $scope, syncData, waitForAuth, growl ) {
  $scope.active = false;
	waitForAuth.then(function(){
		$scope.ref = syncData($scope.loginObj.user.id);
		$scope.posts = $scope.ref.$child('posts');
		$scope.cats = $scope.ref.$child('categories');

	});

	$scope.addCat = function(){
		$scope.cats.$add({id: $scope.newcat, catname: $scope.newcat})
      .then(function(){ growl.addSuccessMessage("Category added!"); });
		$scope.newcat = "";
	};
	$scope.delCat = function(myid){
		$scope.cats.$remove(myid)
      .then(function(){ growl.addWarnMessage("Category deleted!"); });
	};

})//end dashboardctrl

.controller( 'PostListCtrl', function PostListController( $rootScope, $scope ) {


})//end postlistctrl

.controller( 'NewCtrl', function NewPostController( $rootScope, $scope, $location, waitForAuth, $filter, growl ) {


	$scope.post = {};
  waitForAuth.then(function(){
    // console.log( ($filter("orderByPriority")($scope.cats))[0].catname );
    $scope.post.category = ($filter("orderByPriority")($scope.cats))[0].catname;
  });
	$scope.savePost = function(){
		$scope.posts.$add({title: $scope.post.title, content: $scope.post.content, summary: $scope.post.summary, category: $scope.post.category })
      .then( function(p){
			// console.log(p.name());
      $location.path("/dashboard/edit/" + p.name());
        growl.addSuccessMessage("Post saved!");
      });
	};

})//end NewCtrl

.controller( 'EditCtrl', function EditPostController( $rootScope, $scope, $stateParams, $location, waitForAuth, $filter, growl ) {

	$scope.postId = $stateParams.id;
	waitForAuth.then(function(){

    var categories = ($filter("orderByPriority")($scope.cats));
    var catarr = [];
    for(var x = 0; x < categories.length; x++){
      catarr[x] = categories[x].catname;
    }
		$scope.post = $scope.posts.$child($scope.postId);
    $scope.post.$on("loaded", function(){
      //if existing category is not in list, then replace with first item in list
      //also call a set so change is saved to db
      // console.log($scope.post.category," ",catarr[0]);
      if(catarr.indexOf($scope.post.category) < 0){

        $scope.post.category = catarr[0];
        $scope.post.$set({ title: $scope.post.title, content: $scope.post.content, summary: $scope.post.summary, category: $scope.post.category });

      }

    });

	});

	// console.log($scope.cats);
	$scope.savePost = function(){
		$scope.post.$set({ title: $scope.post.title, content: $scope.post.content, summary: $scope.post.summary, category: $scope.post.category })
      .then(function(){ growl.addSuccessMessage("Post edits saved!"); });
	};

	$scope.delPost = function(){
		$scope.post.$remove()
    .then(function(){ growl.addWarnMessage("Post deleted!"); $location.path("/dashboard/postlisting" ); });

	};
})//end editctrl

;//end module
