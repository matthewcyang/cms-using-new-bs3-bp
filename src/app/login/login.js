/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/login`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'mycms.login', [
  'mycms.services',
  'ui.router'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider
  .state( 'login', {
    url: '/login',
    views: {
      "main": {
        controller: 'LoginCtrl',
        templateUrl: 'login/login.tpl.html'
      }
    },
    data:{ pageTitle: 'Login' }
  });
})

.run(['$rootScope', '$location', 'loginService', '$state', 'waitForAuth', function ( $rootScope, $location, loginService, $state, waitForAuth ) {

}])

/**
 * And of course we define a controller for our route.
 */
.controller( 'LoginCtrl', function LoginController( loginService, $scope, $location ) {

  // if( !!(loginService.loggedin()) ){
  //       waitForAuth.then(function(){
          // how to go to manually typed in address?? without this, 
          // login is visible when logged in. with it, manual URL
          // always goes to postlisting
          // $location.path('/dashboard/postlisting');
  //       });
  // }
  if( !!loginService.loggedin() ){
    console.log("login loaded and logged in");
    $location.path('/dashboard/postlisting');
  }
  $scope.doFBLogin = function(){
    loginService.login('facebook', function(e,user){
      if( !e ) {
        console.log("user logged in: ",user);
      }else{
        console.log("error",e);
      }
    });
  };


})

;
