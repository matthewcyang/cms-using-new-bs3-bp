angular.module( 'mycms.postview', [
  'mycms.services',
  'ui.router'
])

.config(function config( $stateProvider ) {
  $stateProvider
  .state( 'postview', {
    abstract: true,
    url: '/:userid',
    views: {
      "main": {
        controller: 'PostsCtrl',
        templateUrl: 'postview/postview.tpl.html'
      }
    },

    data:{ pageTitle: 'posts' }
  })

.state('postview.postlisting', {
    url: '/public/listing/',
    parent: 'postview',
    views:{
      "content": {
        controller: 'PostViewListCtrl',
        templateUrl: "postview/postsummary.tpl.html"
      }
    }
  })

.state('postview.post', {
    url: '/public/post/:id',
    parent: 'postview',
    views:{
      "content": {
        controller: 'PostViewCtrl',
        templateUrl: "postview/singlepost.tpl.html"
      }
    }
  })

.state('postview.category', {
    url: '/public/category/:cat',
    parent: 'postview',
    views:{
      "content": {
        controller: 'CatViewCtrl',
        templateUrl: "postview/catfilter.tpl.html"
      }
    }
  })
  ;//end states

}) //end config

.controller( 'PostsCtrl', function PostsController( $rootScope, $scope, $stateParams, syncData, waitForAuth ) {

    $scope.userid = $stateParams.userid;
  waitForAuth.then(function(){

    $scope.ref = syncData($scope.userid);
    $scope.posts = $scope.ref.$child('posts');
    $scope.cats = $scope.ref.$child('categories');

  });

})//end PostsCtrl

.controller( 'PostViewListCtrl', function PostViewListController(  ){



}) //end PostViewListCtrl

.controller( 'PostViewCtrl', function PostViewController( $scope, $stateParams, waitForAuth  ){

    $scope.postId = $stateParams.id;
    waitForAuth.then(function(){
      $scope.post = $scope.posts.$child($scope.postId);
    });


}) //end PostViewCtrl

.controller( 'CatViewCtrl', function CatViewController( $scope, $stateParams,waitForAuth  ){
  // waitForAuth.then(function(){
  //   console.log( $scope.posts );
  // });
  $scope.search = {};
  $scope.search.category = $stateParams.cat;

}) //end CatViewCtrl

; //end module
