angular.module('mycms.services', [
  'firebase'
])

// a simple utility to create references to Firebase paths
.factory('firebaseRef', ['$firebase', 'FBURL', function( $firebase, FBURL ) {
   /**
    * @function
    * @name firebaseRef
    * @param {String|Array...} path
    * @return a Firebase instance
    */
   return function(path) {
    return new Firebase(pathRef([FBURL].concat(Array.prototype.slice.call(arguments))));
   };
}])

.service('syncData', ['$firebase', 'firebaseRef', function($firebase, firebaseRef) {
  /**
   * @function
   * @name syncData
   * @param {String|Array...} path
   * @param {int} [limit]
   * @return a Firebase instance
   */
  return function(path, limit) {
     var ref = firebaseRef(path);
      if(limit){
        ref = ref.limit(limit);
      } 
     return $firebase(ref);
  };
}])

/**
 * A service that returns a promise object, which is resolved once $firebaseSimpleLogin
 * is initialized.
 *
 * <code>
 *    function(waitForAuth) {
 *        waitForAuth.then(function() {
 *            console.log('auth initialized');
 *        });
 *    }
 * </code>
 */
.service('waitForAuth', function($rootScope, $q, $timeout) {
  function fn(err) {
    if($rootScope.loginObj) {
      $rootScope.loginObj.error = err instanceof Error? err.toString() : null;
    }
    for(var i=0; i < subs.length; i++) { subs[i](); }
    $timeout(function() {
      // force $scope.$apply to be re-run after login resolves
      def.resolve();
    });
  }

  var def = $q.defer(), subs = [];
  subs.push($rootScope.$on('$firebaseSimpleLogin:login', fn));
  subs.push($rootScope.$on('$firebaseSimpleLogin:logout', fn));
  subs.push($rootScope.$on('$firebaseSimpleLogin:error', fn));
  return def.promise;
})

.factory('loginService', ['$rootScope', '$firebaseSimpleLogin', 'firebaseRef', '$timeout',
  function($rootScope, $firebaseSimpleLogin, firebaseRef, $timeout) {
     var auth = null;
     return {
      
        init: function() {
           return auth = $firebaseSimpleLogin(firebaseRef());
           
        },

        login: function(provider, callback) {
           assertAuth();
           auth.$login(provider)
            .then(function(user) {
                 if( callback ) {
                    callback(null, user);
                 }
              }, callback);
        },

        logout: function() {
           assertAuth();
           auth.$logout();
        },

        loggedin: function(){
          if(auth.user == null){
            return false;
          }else{
            return true;
          }
          // return false;
        }

     };

     function assertAuth() {
        if( auth === null ) { throw new Error('Must call loginService.init() before using its methods'); }
     }
  }])

;


function errMsg(err) {
      return err? typeof(err) === 'object'? '['+err.code+'] ' + err.toString() : err+'' : null;
}

function pathRef(args) {
  for(var i=0; i < args.length; i++) {
     if( typeof(args[i]) === 'object' ) {
        args[i] = pathRef(args[i]);
     }
  }
  return args.join('/');
}